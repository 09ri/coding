$(function(){

    //pagetop
    $(window).scroll(function(){
        if($(this).scrollTop() > 200){
            $('#pagetop').css({
                "opacity": 1
            });
        }else{
            $('#pagetop').css({
                "opacity": 0
            });
        }
        var w = $(window).width();
        scrollHeight = $(document).height();
        scrollPosition = $(window).height() + $(window).scrollTop();
        footHeight = $("footer .footer_area").innerHeight();
        if ( scrollHeight - scrollPosition  <= footHeight ) {
            if(w < 640){
                $("#pagetop").css({
                    "bottom": "4vw"
                });
            }else{
                $("#pagetop").css({
                    "bottom": "175px"
                });
            }
        } else {
            $("#pagetop").css({
                "bottom": "20px"
            });
        }
    });

    //smooth scroll
    $('a[href^="#"]').click(function() {
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });


    //parallax kogin
    var kogin_top = $('#xc2020 #kogin .xc_inner').offset().top;
    $(window).scroll(function() {
        var kogin_prlx_value = $(this).scrollTop() - kogin_top;
        var w = $(window).width();
        if (w > 768) {
            $('#xc2020 #kogin .xc_inner .bg_kogin').css('top', kogin_prlx_value / 10 - 150);
        } else {
            $('#xc2020 #kogin .xc_inner .bg_kogin').css('top', kogin_prlx_value / 10);
        }
    });

    //parallax insta
    var insta_top = $('#xc2020 #insta .xc_inner').offset().top;
    $(window).scroll(function() {
        var insta_prlx_value = $(this).scrollTop() - insta_top;
        var w = $(window).width();
        if (w > 768) {
            $('#xc2020 #insta .xc_inner .bg_insta').css('top', insta_prlx_value / 10 - 310);
        } else {
            $('#xc2020 #insta .xc_inner .bg_insta').css('top', insta_prlx_value / 10 - 420);
        }
    });

    //parallax card
    var card_top = $('#xc2020 #card .xc_inner').offset().top;
    $(window).scroll(function() {
        var card_prlx_value = $(this).scrollTop() - card_top;
        var w = $(window).width();
        if (w > 768) {
            $('#xc2020 #card .xc_inner .bg_card').css('top', card_prlx_value / 10 - 550);
        } else {
            $('#xc2020 #card .xc_inner .bg_card').css('top', card_prlx_value / 8 - 350);
        }
    });

    //parallax workshop
    var workshop_top = $('#xc2020 #workshop .xc_inner').offset().top;
    $(window).scroll(function() {
        var workshop_prlx_value = $(this).scrollTop() - workshop_top;
        var w = $(window).width();
        if (w > 768) {
            $('#xc2020 #workshop .xc_inner .bg_workshop').css('top', workshop_prlx_value / 10);
        } else {
            $('#xc2020 #workshop .xc_inner .bg_workshop').css('top', workshop_prlx_value / 10 + 50);
        }
    });

    //parallax showcase
    var showcase_top = $('#xc2020 #showcase .xc_inner').offset().top;
    $(window).scroll(function() {
        var showcase_prlx_value = $(this).scrollTop() - showcase_top;
        var w = $(window).width();
        if (w > 768) {
            $('#xc2020 #showcase .xc_inner .bg_showcase').css('top', showcase_prlx_value / 10 - 700);
        } else {
            $('#xc2020 #showcase .xc_inner .bg_showcase').css('top', showcase_prlx_value / 5 - 400);
        }
    });

});
